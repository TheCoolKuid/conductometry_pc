﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }
       
        public event Start Strt;
        public event Start Stop;
        public event Start ChangeGraphState;
        public event Start ChartBeClosed;
        public event Start ClearSeries;
        public delegate void Start(StartArgs e);
        /// <summary>
        /// Обрабатывает кнопку "Старт"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            StartArgs args = new StartArgs();
            ClearSeries(null);
            double ctv = 0;
            double spd = 0;
            try
            {
                Double.TryParse(this.textBox1.Text, out ctv);
                Double.TryParse(this.textBox2.Text, out spd);
            }catch (Exception exc)
            {
                MessageBox.Show("Введеные значения не являются цифрами!" + exc.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           /* if (spd < 1)
            {
                MessageBox.Show("Минимальная скорость 1 uA/с!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Text = "" + 1;
                spd = 1;
            }*/
            if (spd > 201)
            {
                MessageBox.Show("Максимальная скорость 200 uA/с!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox2.Text = "" + 200;
                spd = 200;
            }
            args.CutoffVoltage = ctv;
            args.Speed = spd;
            args.FileName = this.textBox4.Text;
            Strt(args);
            //isMesuring = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //isMesuring = false;
            Stop(null);
        }

        public void Form1_PlDP(PolDataProfite e)
        {
            Action v = () => this.label1.Text = String.Format("Плотность\n Тока\n {0:f3} А/дм2", (e.current*Math.Pow(10,-6))/(Properties.Settings.Default.radius_el*Properties.Settings.Default.lenght_el*100));
            Action v4 = () => this.label4.Text = String.Format("Записано {0}\n точек из {1}", e.itteration+1, e.PointAmmount);
            try
            {
                Invoke(v);
                Invoke(v4);
            }catch(Exception eSASDSDAF)
            {
                Console.Write(eSASDSDAF.Message);
            }
        }

        private void Form6_FormClosing(object sender, FormClosingEventArgs e)
        {
           if(MessageBox.Show("Вы уверены?", "Ошибка", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    ChartBeClosed(null);
                }
         
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
          
        }
        private bool chartstate = true;
        /// <summary>
        /// кнопка про график
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if(chartstate)
            {
                StartArgs eee = new StartArgs();
                eee.State = false;
                chartstate = false;
                button3.Text = "Показать график";
                ChangeGraphState(eee);
            }
            else
            {
                StartArgs eee = new StartArgs();
                eee.State = true;
                chartstate = true;
                button3.Text = "Убрать график";
                ChangeGraphState(eee);
            }
        }
    }
}
public class StartArgs : EventArgs
{
    public double CutoffVoltage { get; set; }
    public double Speed { get; set; }
    public string FileName { get; set; }
    public bool State { get; set; }
}
