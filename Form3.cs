﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            this.radioButton1.Checked = true;
        }
        public event SaveButtonClick sbc;
        public delegate void SaveButtonClick(ButtonClick e);
        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "csv file (*.csv)|*.csv";
            f.FilterIndex = 1;
            f.RestoreDirectory = true;
            if(f.ShowDialog() == DialogResult.OK)
            {
                this.textBox1.Text = f.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ButtonClick buttonClick = new ButtonClick();
            if(this.radioButton1.Checked)
            {
                buttonClick.SelectedStyle = 0;//origin style
            }
            if (this.radioButton2.Checked)
            {
                buttonClick.SelectedStyle = 1;//excel style
            }
            if (!this.radioButton1.Checked && !this.radioButton2.Checked || this.textBox1.Text.Equals(""))
            {
                MessageBox.Show("Не выбран вариант csv или не указан путь!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if(!this.textBox1.Text.Equals(""))
                {
                    buttonClick.Path = this.textBox1.Text;
                    sbc(buttonClick);
                }
                else
                {
                    MessageBox.Show("Не указан путь!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }

    public class ButtonClick : EventArgs
    {
        public string Path { get; set; }
        public int SelectedStyle { get; set; }
    }
}
