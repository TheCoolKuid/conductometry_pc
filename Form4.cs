﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
        public void ChangeProgresBar(CPBArgs e)
        {
            if (e.SetValue< 257)
            {
                try
                {
                    Action v = () => this.progressBar1.Value = e.SetValue;
                    Action v1 = () => this.label2.Text = String.Format("{0:F1}%", e.SetValue / 257.0 * 100);
                    Invoke(v);
                    Invoke(v1);
                }catch(Exception easas)
                {
                    Console.Write(easas.Message);
                }
               
            }
        }
        public event Calibration1pointStart C1ps;
        public delegate void Calibration1pointStart(CPBArgs e);
        
         private void button1_Click(object sender, EventArgs e)
        {
            C1ps(null);
        }

        private void Form4_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Вы уверены?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
    public class CPBArgs : EventArgs
    {
        public int SetValue { get; set; }
    }
}
