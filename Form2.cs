﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            
        }
        public event IsCalStarted ics;
        public delegate void IsCalStarted(EventArgs e);
        private void Form2_Load(object sender, EventArgs e)
        {
            this.comboBox1.Items.Add(WindowsFormsApp1.Properties.Settings.Default.ComName.ToString());
            this.comboBox1.Items.AddRange(SerialPort.GetPortNames());
            this.textBox1.Text = WindowsFormsApp1.Properties.Settings.Default.SlaveID.ToString();
            this.textBox2.Text = WindowsFormsApp1.Properties.Settings.Default.lenght_el.ToString();
            this.textBox3.Text = WindowsFormsApp1.Properties.Settings.Default.lenght_btw_el.ToString();
            this.textBox4.Text = WindowsFormsApp1.Properties.Settings.Default.RShunt.ToString();
            this.textBox5.Text = WindowsFormsApp1.Properties.Settings.Default.radius_el.ToString();
            this.numericUpDown1.Value = Properties.Settings.Default.AutosavePeriod / 60000;
            this.comboBox1.SelectedIndex = 0;
            checkBox1.Checked = Properties.Settings.Default.SameNumberOfSamples;
            numericUpDown2.Value = Properties.Settings.Default.NumberOfSamples;
            this.checkBox2.Checked = Properties.Settings.Default.AutoDeleteEjection;
            this.checkBox3.Checked  = Properties.Settings.Default.AutoCalcStat;
        }
        /// <summary>
        /// Обработка кнопки "сохранить"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            String port_name = this.comboBox1.Items[this.comboBox1.SelectedIndex].ToString();
            byte slaveid = 0;
            double lengt = 0;
            double lbel = 0;
            double diam = 0;
            uint rshunt = 0;
           
            bool sl_res = Byte.TryParse(this.textBox1.Text, out slaveid);
            bool lenght = Double.TryParse(this.textBox2.Text, out lengt); //H
            bool lenght_between_el = Double.TryParse(this.textBox3.Text, out lbel); //L
            bool el_diametr = Double.TryParse(this.textBox5.Text, out diam); //A
            bool r_res = UInt32.TryParse(this.textBox4.Text, out rshunt);
            
            String result = "";
            if(!sl_res)
            {
                result += "Неверное ID!";
            }
            if (!lenght)
            {
                result += "Неверно указана длина погруженной части!";
            }
            if (!lenght_between_el)
            {
                result += "Неверно указано расстояние между электродами!";
            }
            if (!el_diametr)
            {
                result += "Неверно указан радиус электрода!";
            }
            if (!r_res)
            {
                result += "Неверное R!";
            }
            if(!sl_res || !lenght_between_el || !el_diametr || !lenght || !r_res)
            {
                DialogResult res = MessageBox.Show(result, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                WindowsFormsApp1.Properties.Settings.Default.SlaveID = slaveid;
                WindowsFormsApp1.Properties.Settings.Default.lenght_el = lengt;
                WindowsFormsApp1.Properties.Settings.Default.lenght_btw_el = lbel;
                WindowsFormsApp1.Properties.Settings.Default.radius_el = diam;
                WindowsFormsApp1.Properties.Settings.Default.RShunt = rshunt;
                WindowsFormsApp1.Properties.Settings.Default.ComName = port_name;
                Properties.Settings.Default.SameNumberOfSamples = checkBox1.Checked;
                Properties.Settings.Default.NumberOfSamples = (int)numericUpDown2.Value;
                Properties.Settings.Default.AutosavePeriod = (int)this.numericUpDown1.Value * 60000;
                Properties.Settings.Default.AutoDeleteEjection = this.checkBox2.Checked;
                Properties.Settings.Default.AutoCalcStat = this.checkBox3.Checked;
                try
                {
                    WindowsFormsApp1.Properties.Settings.Default.Save();
                    DialogResult res = MessageBox.Show("Настройки сохранены!", "Успешно", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if(res == DialogResult.OK)
                    {
                        WindowsFormsApp1.Properties.Settings.Default.isFirstStartUp = false;
                       //this.Close();
                    }
                }
                catch(Exception ex)
                {
                    DialogResult res = MessageBox.Show("Ошибка: "+ ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        /// <summary>
        /// Обработка кнопки "Справка"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Обработка кнопки "Калибровка"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            EventArgs ebat = new EventArgs();
            ics(ebat);
        }
        /// <summary>
        /// Обрабатывает переключатель включения режима записи одинаковых точек для всех измерений
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(this.checkBox1.Checked)
            {
                numericUpDown2.Enabled = true;
            }
            else
            {
                numericUpDown2.Enabled = false;
            }
        }
    }
}
