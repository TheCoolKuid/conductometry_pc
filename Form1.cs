﻿using MathNet.Numerics;
//using //Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO.Compression;
/**
* TODO: Через жопу переводится в json
* TODO: припаять между R16 и переходным отверстием конденсатор 680 пф.
* по факту ОУ нихуя не r2r(при напряжении питания показывают хуету), Вообще, проблема в шунте и низком
* напряжении питания. То есть уже при 300 мка падение на шунте 3В, а значит максимальное сопротивление которое
* можно измерить - 6666 Ом. Нужно поменять опорное и сделать максимальный ток - до 200мкА, причем суммарное
* падение на шунте+раствор не должно превышать 4В, т.к. оу нихуя не r2r
* todo: Изменил схему, теперь vref4 3,072 (на деле 3,14), а вреф3 постояннотоковый, при такой конфигурации особого смысла в 
* конденсаторе нет. Обязательно нужно добавить обратную связь по току, потому что при допуске в 30% ток непредсказуем!!!
* todo: поставил на вход ацп RC фильтр 470пф/5кОм, сделал самокалибровку.
* Необходимо реализовать режим калибровки силы тока. Алгоритм:
* 1)Соединить ЕО- с ЕО+, соединить ЕI+ с ЕО-, EI- соединить с землей, тогда потенциал шунта вычтется из VREF3 и можно определять
* силу тока для каждого кода.(как я понимаю до примерно 30-40мка оу будет слать нас в пизду, поэтому будет не очень достоверно)
* 
* Калибровку реализовал. Каким то хуем число 216 вызывает зависание прибора. Я не ебу и ебать не хочу, ебись оно конем
* Результат калибровки сохраняется в CurrentCal как json строка
* Необходимо реализовать загрузку этого значения из памятию
* Необходимо реализовать проверку на калибровку (типо калибровать раз в сутки, если откалиброван - использовать значения из
* realcurent (типо из памяти их загружать или калибровать) а если давно не калибровали использовать посчитанные значения
* 
* Вроде все что выше сделал. Сделал так же автообновление фона(нужно проверить)
* Начал делать выбор тока (см че наделал), но не доделал
* надо сделать
* 
* Собрал новую версию прибора. Переделал механизм калибровки
* Придется увеличивать сопротивление шунта до 100Ком, из за того
* что на электродах протекает электролиз, который смещает потенциал,
* причем этот потенциал начинает быть непредсказуемым. Это очень плохо,
* для 100 мка при площади электрода в 1см2 плотность тока 0.01A/дм2! а при 100 0.001А/дм2
* 
* Установил новый шунт на 100Ком (99.8 5%, надо поменять на нормальный)
* Сделал окошко со списком измерений, прямо в окне можно задавать название каждому измерению
* это удобно для обработки
* Поменял механизм измерения сопротивления:
* Если представить зависимость U от I
* U = n(catode) + n(anode) + f(dif) + f(pol) +  IR
* n - скачек потенциала на границе электрод/раствор
* f(dif) - потенциал, возникающий при различных подвижностях ионов в растворе (типо слева быстые ионы +, а справа медленные -, то тогда 
* будет возникать диффузионный потенциал)
* f(pol) - поляризация электрода
* и что то неучтенное...
* При I +- коннстанта (я сделал интервал +-3мкА от 10мкА) все эти потенциалы теоретически константы,
* тогда задавая I и измеряя U, тангенс угла наклона кривой U/I есть сопротивление, а intercept - сумма потенциалов,
* таким образом, я задаю ток от примерно 7 до 13 мкА, измеряю потенциал и вычисляю тангенс угла наклона, который и является
* искомым сопротивлением. На обыкновенном резисторе работает, нужно тестировать на растворахъ
* П.С. я хз, может быть что то и сломал, но калибровка работает адекватно
* TODO: протестировать на растворах, сделать калибровку ячейки (где бы надыбать КСЛ), поискать оишбки!
*  * */

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //локальные переменные
        Communication c;
        System.Timers.Timer poll_timer;
        System.Timers.Timer autosave_timer;
        System.Timers.Timer StatusTimer;
        bool isMesStarted = false;
        Queue<int> MesuredData = new Queue<int>();
        //конец локальных переменных

        private void Form1_Load(object sender, EventArgs e)
        {
            c = new Communication();
           if(WindowsFormsApp1.Properties.Settings.Default.isFirstStartUp)
            {
                Form2 f = new Form2();
                f.ShowDialog();
            }
            //Настраиваем прерывания
            c.msch += C_msch;
            c.profit_data += C_profit_data;
            c.cd_resived += C_cd_recived;
            Properties.Settings.Default.PropertyChanged += Default_PropertyChanged;
            calculateCurrent();
            bool comres = c.setupCOM();
            if(comres)
            {
                this.pictureBox1.Image = Properties.Resources.connected;
                this.Text = "Подключено к "+Properties.Settings.Default.ComName;
            }
            else
            {
                this.pictureBox1.Image = Properties.Resources.dscn;
                this.Text = "Ошибка подключения к " + Properties.Settings.Default.ComName;
            }
            //Отслеживаем нажатие клавиш
            this.KeyPreview = true;
            this.KeyDown += Form1_KeyDown;
            /*Таймер, который будет опрашивать МК каждую секунду*/
            poll_timer = new System.Timers.Timer(1000/(WindowsFormsApp1.Properties.Settings.Default.UpdateFreq/5));
            //poll_timer = new System.Timers.Timer(200);
            poll_timer.AutoReset = true;
            poll_timer.Enabled = false;
            poll_timer.Elapsed += Poll_timer_Elapsed;
            /* Таймер на автосохранение*/
            autosave_timer = new System.Timers.Timer(Properties.Settings.Default.AutosavePeriod);//В мс
            autosave_timer.AutoReset = true;
            autosave_timer.Enabled = false;
            autosave_timer.Elapsed += Update_timer_Elapsed1;
            //Таймер, вызывающий калибровку фона
            /*background_timer = new System.Timers.Timer(Properties.Settings.Default.AutoBCG);//В мс
            background_timer.AutoReset = true;
            background_timer.Enabled = false;
            background_timer.Elapsed += Background_timer_Elapsed; ;*/
            //ReadCalCurrent();
            //sendCurrent(127);//устанавливаем среднеезначение
            StatusTimer  = new System.Timers.Timer(5000);//В мс
            StatusTimer.AutoReset = true;
            StatusTimer.Enabled = false;
            StatusTimer.Elapsed += StatusTimer_timer_Elapsed; ;
        }
        /// <summary>
        /// Опрашивает главный регистр статуса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StatusTimer_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            byte[] b = { 0x11, 0x01, 0, 0, 0, 0x10, 0, 0 }; //Читаем статус регистр
            int crc = c.CRC16(b, 6);
            b[6] = (byte)(crc >> 8);
            b[7] = (byte)(crc);
            c.sendData(b, 8);
        }

        /// <summary>
        /// Обрабатывает нажатия клавиш
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
           if(e.KeyCode == Keys.F5)
            {
                button1_Click(null, null); //Изменить состояние измерения
            }
           if(e.Control && e.KeyCode == Keys.S)
            {
                button2_Click(null, null); //Сохранение
            }
        }

        byte previous_code = 0;
       
        /// <summary>
        /// Обрабатывает тик таймера автосохранения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Update_timer_Elapsed1(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!IsCurrentCalOccur)
            {
                try
                {
                    Action v1 = () => this.label7.Text = "Сохранение...";
                    Invoke(v1);
                    bool isTimerWasEnabled = false;
                    if (poll_timer.Enabled)
                    {
                        poll_timer.Stop();
                        isTimerWasEnabled = true;
                    }
                    List<int> mte = new List<int>(); 
                    for(int i = 0; i < output_buffer.Count; i++)
                    {
                        mte.Add(i);
                    }
                    Directory.CreateDirectory(String.Format(@"autosave/{0}", DateTime.Now.ToShortDateString()));
                    ExportMeasurment(false, String.Format(@"./autosave/{0}/Conductometry_{0}_autosave.csv", DateTime.Now.ToShortDateString()), false, mte);
                    if (isTimerWasEnabled)
                    {
                        poll_timer.Start();
                    }
                    Action v2 = () => this.label7.Text = "";
                    Invoke(v2);
                }
                catch (Exception ebat)
                {
                    Action v = () => this.label7.Text = "Ошибка при сохранении!" + ebat.Message;
                    Invoke(v);
                } 
            }
        }
        /// <summary>
        /// Пересчитывает ток при изменеии параметров
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Default_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (!IsCurrentCalOccur)
            {
                Current.Clear();
                calculateCurrent();
                bool isatswasdisabled = false;
                if (autosave_timer.Enabled)
                {
                    autosave_timer.Stop();
                    isatswasdisabled = true;
                }
                autosave_timer.Interval = Properties.Settings.Default.AutosavePeriod;
                if (isatswasdisabled)
                {
                    autosave_timer.Start();
                }
                isatswasdisabled = false;
              /*  if (background_timer.Enabled)
                {
                    background_timer.Stop();
                    isatswasdisabled = true;
                }
                background_timer.Interval = Properties.Settings.Default.AutoBCG;
                if (isatswasdisabled)
                {
                    background_timer.Start();
                }*/
            }
        }
        /// <summary>
        /// Выводит значение тока на приборе
        /// </summary>
        /// <param name="e"></param>
        private void C_cd_recived(ProfitDataArgs e)
        {
            if (!IsCurrentCalOccur)
            {
                if (!isPolarMes)
                {
                    if ((byte)e.data[0] == sent_current)
                    {
                        if((byte)e.data[0] == last_code - 39 + CTIRL_itterator*13) //Проверяем установился ли заданный ток
                        {
                            //if((byte) e.data[0] == last_code - 39)
                           // {
                               // poll_timer.Start();
                            //}
                            //IsCurrentSet = true;
                        }
                        else
                        {
                            //IsCurrentSet = false;
                        }
                        Action v = () => this.label4.Text = String.Format("A = {0:F2} uA", getCurrent(e.data[0]));
                        Invoke(v);
                    }
                    else
                    {
                        sendCurrent(sent_current);
                    } 
                }
                if (isPolarMes)
                {
                    if ((byte)e.data[0] == sent_current)
                    {
                        //if (!CalibrationTimer.Enabled)
                        //{
                           // CalibrationTimer.Start();
                       // }
                    }
                    else
                    {
                        sendCurrent(sent_current);
                    }
                }
                if(BackgroundCal)
                {
                    if ((byte)e.data[0] == sent_current)
                    {
                        Thread.Sleep(500);
                        Poll_timer_Elapsed(null, null);
                    }
                    else
                    {
                        sendCurrent(sent_current);
                    }
                }
            }
        }

        byte sent_current = 127;
        /// <summary>
        /// Отправляет значение тока в прибор
        /// </summary>
        /// <param name="current"></param>
        private void sendCurrent(byte current)
        {
                byte current_code = current;
                if(current_code == 216)
                {
                    current_code = 215;
                }
                sent_current = current_code;
                byte[] poll = { WindowsFormsApp1.Properties.Settings.Default.SlaveID, 0x06, 0,0 ,0, current_code, 0,0};
                int crc = c.CRC16(poll, 6);
                poll[6] = (byte)(crc >> 8);
                poll[7] = (byte)(crc);
                bool res = c.sendData(poll, 8);
                if(!res)
                {
                    this.label4.Text = "Ошибка при отправке";
                }
        }
        Background bc = new Background();
        Background LastSetVal = new Background();
        List<List<Background>> output_buffer = new List<List<Background>>();
        /// <summary>
        /// Выводит поступившую информацию на табло
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Update_timer_Elapsed(Background result)
        {
            double voltage = result.voltage;
            double dvoltage = result.dvoltage;
            double current = result.Current;
            double impedance = result.impedance;
            //double conductivity = Properties.Settings.Default.lenght_btw_el/(Properties.Settings.Default.lenght_el*Properties.Settings.Default.radius_el * impedance);
            double conductivity = result.conductivity;
            //сохраняем значение в выводной буфер
            Background bcc = new Background();
            bcc.voltage = voltage;
            bcc.dvoltage = dvoltage;
            bcc.impedance = impedance;
            bcc.conductivity = conductivity;
            bcc.Current = current;
            bcc.Temperater = result.Temperater;
            bool isntoutliner = true;
            if (Properties.Settings.Default.AutoDeleteEjection) //Проверка на выброс
            {
                isntoutliner = GrubbsTest(bcc);
                if(isntoutliner)
                {
                    output_buffer[CurrentValueCell].Add(bcc);
                }
            }
            else //без проверки добавляем любое значение
            {
                output_buffer[CurrentValueCell].Add(bcc);
            }
                Action vadim = () => this.label2.Text = String.Format("Вне диапазона");
                Action SetAnalogScale = () =>
                {
                    this.label10.Text = "1 См/М";
                    this.label8.Text = "1 КСм/М";
                    this.label11.Text = "500 См/М";
                    this.progressBar1.Value = (int)Math.Round(conductivity / 10); //1000 - 100, conductivity - x
                }; 
                if (impedance < 10000)
                {
                    vadim = () => this.label2.Text = String.Format("{0:F2} Ом", impedance - bc.impedance);
                }
                if (impedance >= 10000 && impedance < 1000000)
                {
                    vadim = () => this.label2.Text = String.Format("{0:F2} КОм", (impedance - bc.impedance) / 1000);
                }
                if (impedance >= 1000000)
                {
                    vadim = () => this.label2.Text = String.Format("{0:F2} МОм", (impedance - bc.impedance) / 1000000);
                }
                Action vadim2 = () => this.label1.Text = String.Format("Вне диапазона");
                if (conductivity >= 1)
                {
                    vadim2 = () => this.label1.Text = String.Format("{0:F2} См/м", conductivity - bc.conductivity);
                }
                if (conductivity < 1 && conductivity >= 0.001)
                {
                    vadim2 = () => this.label1.Text = String.Format("{0:F2} мСм/м", (conductivity - bc.conductivity) * 1000);
                }
                if (conductivity < 0.001)
                {
                    vadim2 = () => this.label1.Text = String.Format("{0:F2} uСм/м", (conductivity - bc.conductivity) * 1000000);
                }
                SetAnalogScale = () =>
                {
                    if (conductivity > 0 && conductivity != Double.PositiveInfinity)
                    {
                        int power = (int)Math.Round(Math.Log10(conductivity)); //считаем десятичный логарифм от проводимости
                        int up = power + 1;
                        this.label10.Text = String.Format("1*10^{0} См/М", power); //мин
                        this.label8.Text = String.Format("1*10^{0} См/М", up); //макс
                        this.label11.Text = String.Format("5*10^{0} См/М", power); //середина
                        this.progressBar1.Value = (int)Math.Round(conductivity * 100 / Math.Pow(10.0, up)); //Math.Pow(10.0, up) - 100, conductivity - x
                    }
                };
            Action set_temp = () => label12.Text = String.Format("{0:F1} °C", result.Temperater/10);
            string vdv = String.Format("Колличество точек:{0}", output_buffer[CurrentValueCell].Count);
                Action v = () => this.label3.Text = vdv;
                Action vkl = () =>
                {
                    int rest = output_buffer[CurrentValueCell].Count % Properties.Settings.Default.NumberOfSamples;
                    if (Properties.Settings.Default.SameNumberOfSamples && rest == 0 && isMesStarted)
                    {
                        button1_Click(null, null);
                    }
                };
                try
                {
                Action changeColor = () =>
                {//возвращаем цвет к стандартному
                    this.label1.ForeColor = Color.DarkGreen;
                    this.label2.ForeColor = Color.Black;
                    if (Properties.Settings.Default.AutoDeleteEjection) //Проверка на выброс
                    {
                        if(!isntoutliner)//выделяем выброс
                        {
                            this.label1.ForeColor = Color.DarkRed; //проводимость
                            this.label2.ForeColor = Color.DarkRed; //сопротивление
                        }
                    }
                };
                Action setStat = () =>
                {
                    if (Properties.Settings.Default.AutoCalcStat)
                    {
                        CalcStat(); //Считаем статистику
                    }
                };
                Invoke(SetAnalogScale);
                Invoke(setStat);
                Invoke(v);
                Invoke(vadim);
                Invoke(vadim2);
                Invoke(vkl);
                Invoke(changeColor);
                Invoke(set_temp);
            }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            
        }
        /// <summary>
        /// Высчитывает среднее сопротивление, ошибку и заносит в таблицу
        /// </summary>
        private void CalcStat()
        {
            if (output_buffer[CurrentValueCell].Count > 3)
            {
                List<double> cond = new List<double>();
                foreach (Background b in output_buffer[CurrentValueCell])
                {
                    cond.Add(b.conductivity); //Вытягиваем значения
                }
                double cond_summ = 0;
                foreach (double d in cond) //Считаем (xi-xavg)^2
                {
                    cond_summ += Math.Pow(d - cond.Average(), 2);
                }
                double sko = Math.Sqrt(cond_summ / (cond.Count - 1));//Считаем СКО
                Chart ch = new Chart();
                double error = sko * ch.DataManipulator.Statistics.InverseTDistribution(0.05, cond.Count - 1); //Считаем ошибку
                this.dataGridView1.Rows[CurrentValueCell].Cells[3].Value = String.Format("{0:E3}", cond.Average());
                this.dataGridView1.Rows[CurrentValueCell].Cells[4].Value = String.Format("{0:E3}", error);

            }
        }
        /// <summary>
        /// Проводит тест грабса
        /// </summary>
        /// <param name="bb">подозреваемое значение</param>
        /// <returns>true - если не выброс, false если выброс</returns>
        private bool GrubbsTest(Background bb)
        {
            if (output_buffer[CurrentValueCell].Count > 3 )
            {
                if (bb.impedance > 0 && bb.conductivity > 0)
                {
                    List<double> cond = new List<double>();
                    List<double> imp = new List<double>();
                    foreach (Background b in output_buffer[CurrentValueCell]) //Собираем массивы с данными
                    {
                        cond.Add(b.conductivity);
                        imp.Add(b.impedance);
                    }
                    double summ_cond = 0;
                    double summ_imp = 0;
                    foreach (double d in cond) //Считаем (xi-xavg)^2
                    {
                        summ_cond += Math.Pow(d - cond.Average(), 2);
                    }
                    foreach (double d in imp)
                    {
                        summ_imp += Math.Pow(d - cond.Average(), 2);
                    }
                    double s_cond = Math.Sqrt(summ_cond / (cond.Count - 1)); // считаем СКО
                    double s_imp = Math.Sqrt(summ_imp / (imp.Count - 1));

                    double U_cond = (bb.conductivity - cond.Average()) / s_cond; //Считаем критерий граббса
                    double U_imp = (bb.conductivity - cond.Average()) / s_cond;
                    if (U_cond < getGrubbsCoef(cond.Count) && U_imp < getGrubbsCoef(imp.Count)) //Если оба критерия меньше коэфициента
                    {
                        return true; //не выброс
                    }
                    else
                    {
                        return false; //выброс
                    }
                }
                else
                {
                    return false; //проверка на меньше нуля
                }
            }
            else
            {
                return true; //первые три пропускааем
            }
        }
        /// <summary>
        /// Подгоняет падение напряжения до 1В
        /// Сначала пытается подогнать dV в область 2-3V
        /// Далее пытается более точно подогнать падение в диапазон 2.2/2.4В
        /// Подгоняет ток в область 40-255. Если не получается, то оставляет значение в область 2-3В
        /// </summary>
        /// <param name="result"></param>
        private void AutoMode(int result)
        {
            // Первично пытаемся добиться падения в диапазоне 1-2В(V от 1.3(1600)(dv - 2) до 2.3(2800)(dv - 1))
            //Проверяем, если ток ниже 1.3
            double v = result * (Communication.CurrentVDDA / 4095);
            double aprox_impedance = ((result * (Communication.CurrentVDDA / 4095)) / getCurrent(sent_current)) * 1000000;//в омах
            if (v < 2)//Если напряжение меньше 2В, понижаем ток
            {
                double dv = 2 - v;//в вольтах
                double deltaA = (dv / aprox_impedance) * 1000000; //в мкА
                int sending_code = sent_current - (int)Math.Round(deltaA / dA);
                byte send = 0;
                if (sending_code > -1)
                {
                    send = Convert.ToByte(sending_code);
                }
                else
                {
                    send = 0;
                    MessageBox.Show(String.Format("Программа не смогла найти оптимальное значение тока. Ток установлен на {0} мкА, установка тока переведена в ручной режим.", getCurrent(send)), "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Properties.Settings.Default.isModeAuto = false;
                    Action v2 = () => this.label6.Text = "Режим: Ручной";
                    Invoke(v2);
                }
                sendCurrent(send);
                /*byte sending_byte = 0;
                try
                {
                    if (1600 - result <= 100) //Если разница между V 1,289 (dv 2,01) <= 80мВ
                    {
                        sending_byte = (byte)((int)sent_current - 5);
                    }
                    if (1600 - result <= 496 && 1600 - result > 100) //Если разница между V 1,289 (dv 2,01) от 80 до 400мВ
                    {
                        sending_byte = (byte)((int)sent_current - 15);
                    }
                    if (1600 - result <= 992 && 1600 - result > 496) //Если разница между V 1,289 (dv 2,01) от 400мВ до 800мВ
                    {
                        sending_byte = (byte)((int)sent_current - 30);
                    }
                    if (1600 - result > 992) //Если разница между V 1,289 (dv 2,01) больше 800мВ
                    {
                        sending_byte = (byte)((int)sent_current - 45);
                    }
                    sendCurrent(sending_byte);
                }
                catch (ArgumentOutOfRangeException e)
                {
                    sending_byte = 127;
                    sendCurrent(sending_byte);
                    MessageBox.Show(String.Format("Программа не смогла найти оптимальное значение тока. Ток установлен на {0} мкА, установка тока переведена в ручной режим.", Current[sending_byte]), "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }*/
            }
            //проверяем если потенциал выше 3В, повышаем ток
            if(v > 3)
            {
                double dv = v - 3;//в вольтах
                double deltaA = (dv / aprox_impedance) * 1000000; //в мкА
                int sending_code = sent_current + (int)Math.Round(deltaA / dA);
                byte send = 0;
                if (sending_code < 255)
                {
                    send = Convert.ToByte(sending_code);
                }
                else
                {
                    send = 255;
                    MessageBox.Show(String.Format("Программа не смогла найти оптимальное значение тока. Ток установлен на {0} мкА, установка тока переведена в ручной режим.", getCurrent(send)), "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Properties.Settings.Default.isModeAuto = false;
                    Action v3 = () => this.label6.Text = "Режим: Ручной";
                    Invoke(v3);
                }
                sendCurrent(send);
               /* byte sending_byte = 0;
                try
                {
                    if (result - 2800 <= 100) //Если разница между V 2,255 (dv 1) <= 80мВ
                    {
                        sending_byte = (byte)((int)sent_current + 5);
                    }
                    if (result - 2800 <= 496 && result - 2800 > 100) //Если разница между V 2,255 (dv 1) от 80 до 400мВ
                    {
                        sending_byte = (byte)((int)sent_current + 15);
                    }
                    if (result - 2800 <= 992 && result - 2800 > 496) //Если разница между V 2,255 (dv 1) от 400мВ до 800мВ
                    {
                        sending_byte = (byte)((int)sent_current + 30);
                    }
                    if (result - 2800 > 992) //Если разница между V 2,255 (dv 1)больше 800мВ
                    {
                        sending_byte = (byte)((int)sent_current + 45);
                    }
                    sendCurrent(sending_byte);
                }
                catch(ArgumentOutOfRangeException e)
                {
                    sending_byte = 127;
                    sendCurrent(sending_byte);
                    MessageBox.Show(String.Format("Программа не смогла найти оптимальное значение тока. Ток установлен на {0} мкА, установка тока переведена в ручной режим.", Current[sending_byte]), "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                } **/
            }
            if(v > 2 && v < 3) //Результат между 3 и 2 В
            {
                if (!(v > (Properties.Settings.Default.VREF3 - 1) - 0.1 && v < (Properties.Settings.Default.VREF3 - 1) + 0.1))//Если dv ~1В
                {
                    if (sent_current > 40 && sent_current < 255)//~50 мкА до 314ы мка
                    {
                        double dv = v - (Properties.Settings.Default.VREF3 - 1); //Находим разницу между серединой диапазона и текущим значением
                        double deltaA = (dv / aprox_impedance) * 1000000;//В мка
                        int dcode = (int)Math.Round(deltaA / dA);
                        if ((sent_current + dcode) > 40 && (sent_current + dcode) < 255)
                        {
                            sendCurrent((byte)(sent_current + dcode));
                        }
                        else
                        {
                            Properties.Settings.Default.isModeAuto = false;
                            Action v1 = () => this.label6.Text = "Режим: Ручной";
                            Invoke(v1);
                            /*if ((sent_current + dcode) <= 40)
                            {
                                if (MessageBox.Show("Найденное программой значение тока не входит в рабочий диапазон. Использовать его?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    sendCurrent((byte)(sent_current + dcode));
                                }
                                else
                                {
                                    sendCurrent(40);
                                    Properties.Settings.Default.isModeAuto = false;
                                     Action v = () => this.label6.Text = "Режим: Ручной";
                                    Invoke(v);
                                }
                            }
                            if ((sent_current + dcode) >= 167)
                            {
                                if (MessageBox.Show("Найденное программой значение тока не входит в рабочий диапазон. Использовать его?", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    sendCurrent((byte)(sent_current + dcode));
                                }
                                else
                                {
                                    sendCurrent(167);
                                    Properties.Settings.Default.isModeAuto = false;
                                    Action v = () => this.label6.Text = "Режим: Ручной";
                                    Invoke(v);
                                }
                            }
                            */
                        }
                    }
                    else
                    {
                        byte sending_byte = 127;
                        sendCurrent(sending_byte);
                        MessageBox.Show(String.Format("Найденное программой значение тока, скорее всего, недостоверно. Ток установлен на {0:F0} мкА, установка тока переведена в ручной режим.", getCurrent(sending_byte)), "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Properties.Settings.Default.isModeAuto = false;
                        Action vvv = () => this.label6.Text = "Режим: Ручной";
                        Invoke(vvv);
                    }
                }
                else
                {
                    //Properties.Settings.Default.isModeAuto = false;
                    //Action vv = () => this.label6.Text = "Режим: Ручной";
                    //Invoke(vv);
                }
            }
        }
        int mean_itter = 0;
        List<double> temp_values = new List<double>();
        /// <summary>
        /// Добавляет пришедшую инфромацию в очередь
        /// </summary>
        /// <param name="e"></param>
        private void C_profit_data(ProfitDataArgs e)
        {
           Background b = new Background();
            double imp_average = 0;
            double temp_average = 0;
            int vdda = e.data[7];
            int rshunt = e.data[15] << 16 | e.data[16];
            for(int i = 0; i < 7; i++)
            {
                imp_average += e.data[i];
            }
            imp_average /= 7;
            for (int i = 0; i < 7; i++)
            {
                temp_average += e.data[i + 8];
            }
            temp_average /= 7;
           b.impedance = imp_average * (vdda / 8192.0) //расчитывает сопротивление по новому
           / (10 * Properties.Settings.Default.VREF4*1000 / rshunt / 255);
            b.conductivity = getConstant() / b.impedance;
            b.Temperater = temp_average;
           Update_timer_Elapsed(b);                    
        }

        private bool[] ignore_error = new bool[3];
        /// <summary>
        /// Включает/Выключает измерения по ответу МК
        /// </summary>
        /// <param name="e"></param>
        private void C_msch(MesureStatusChangeArgs e)
        {
           if(e.isMesEnable)
            {
                Action v = () => this.button1.Text = "Завершить измерение";
                Action v1 = () => poll_timer.Start();
                Action v22 = () => StatusTimer.Start();
                ignore_error[0] = false;
                ignore_error[1] = false;
                ignore_error[2] = false;
                //добавляем новую ячейку для измерения
                List<Background> Slot = new List<Background>();
                CurrentValueCell++;
                Action l9 = () => this.label9.Text = String.Format("Измерение №{0}", CurrentValueCell + 1);
                Action l3 = () => this.label3.Text = String.Format("Количество точек:{0}", 0);
                Action l33 = () => this.dataGridView1.Rows.Add(String.Format("Измерение №{0}", CurrentValueCell + 1), String.Format("{0}", CurrentValueCell + 1), true);
                try
                {
                    Invoke(v);
                    Invoke(v1);
                    Invoke(v22);
                    Invoke(l9);
                    Invoke(l3);
                    Invoke(l33);
                }
                catch (Exception exs)
                {
                    Console.Write(exs.Message);
                }
                output_buffer.Add(Slot);
                //CalculateTanOfIRLine(-1, 0); //Запускаем измерение
                isMesStarted = true;
             }
           if(e.isMesDisable)
            {
                Action v = () => this.button1.Text = "Начать измерение";
                Action v1 = () => poll_timer.Stop();
                Action v3 = () => StatusTimer.Stop();
                try
                {
                    Invoke(v);
                    Invoke(v1);
                    Invoke(v3);
                    
                }
                catch (Exception exs)
                {
                    Console.Write(exs.Message);
                }
                isMesStarted = false;
            }
            String error_message = "";
            Action v2 = () =>
            {
                if(MessageBox.Show(error_message, "Ошибка", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error) == DialogResult.Ignore)
                {
                    ignore_error[e.error_code-1] = true; //Кнопка "пропустить" отключит показ ошибок
                }
            };
            switch (e.error_code) //Выводим сообщения об ошибках, если они есть
            {
                case 3: //Электрод украли
                    error_message = Properties.Settings.Default.ErrorMessage3;//Сообщаем об ошибки
                    if(!ignore_error[2])
                        Invoke(v2);
                    break;
                case 2: //Проблемы с таск менеджером
                    error_message = Properties.Settings.Default.ErrorMessage2;//Сообщаем об ошибки
                    if (!ignore_error[1])
                        Invoke(v2);
                    break;
                case 1: //Проблемы с I2C
                    error_message = Properties.Settings.Default.ErrorMessage1;//Сообщаем об ошибки
                    if (!ignore_error[0])
                        Invoke(v2);
                    break;
            }
        }
        /// <summary>
        /// Опрашивает МК на результаты с установленной частотой
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Poll_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (IsCurrentSet)
            //{
            byte[] b = { 0x11, 0x04, 0, 0, 0, 0x11, 0, 0 }; //Запрашиваем с 0 до 15 регистры
            int crc = c.CRC16(b, 6);
            b[6] = (byte)(crc >> 8);
            b[7] = (byte)(crc);
            c.sendData(b, 8);
        }
        /// <summary>
        /// Обрабатывает кнопку "Настройки"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ics += Form2_ics;
            form2.ShowDialog();
        }
        public event pgbarshouldchange pgbsc;
        public delegate void pgbarshouldchange(CPBArgs e);
        private bool IsCurrentCalOccur = false;
        private bool IsElectrodesConnected = false;
        private int current_cal_itterator = 0;
        private bool isTimerWasDisabled = false;
        private List<double> RealVoltage = new List<double>();
        private List<double> RealCode = new List<double>();
        /// <summary>
        /// Обрабатывает окно калибровки
        /// </summary>
        /// <param name="e"></param>
        private void Form2_ics(EventArgs e)
        {
        
                Form4 form4 = new Form4();
                this.pgbsc += form4.ChangeProgresBar;
                form4.C1ps += Form4_C1ps;
                form4.ShowDialog();
            
        }
        /// <summary>
        /// Обрабатывает нажатие кнопки Старт в окне калибровки
        /// </summary>
        /// <param name="e"></param>
        private void Form4_C1ps(CPBArgs e)
        {
            if (RealVoltage != null)
            {
                RealVoltage.Clear();
                RealCode.Clear();
            }
            //CalibrationTimer = new System.Timers.Timer(200);
           // CalibrationTimer.Interval = 200;
           // CalibrationTimer.AutoReset = true;
            //CalibrationTimer.Elapsed += CalibrationTimer_Elapsed;
            IsCurrentCalOccur = true;
            IsElectrodesConnected = true;
            if (poll_timer.Enabled == true)
            {
                isTimerWasDisabled = true;
                poll_timer.Stop();
            }
            if (!isMesStarted)
            {
                byte[] b = { 0x11, 0x05, 0, 0, 0xFF, 0, 0, 0 };
                int crc = c.CRC16(b, 6);
                b[6] = (byte)(crc >> 8);
                b[7] = (byte)(crc);
                c.sendData(b, 8); 
            }
            sendCurrent(0);
            //CalibrationTimer.Start();
        }
        /// <summary>
        /// Таймер калибровки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CalibrationTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //sendCurrent((byte)current_cal_itterator);
            Poll_timer_Elapsed(null, null);
        }

        /// <summary>
        /// Обрабатывает двойной клик по сообщению об ошибке КОМ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label5_DoubleClick(object sender, EventArgs e)
        {
            if (!c.isCOMopen())
            {
                bool comres = c.setupCOM();
                if (comres)
                {
                    this.Text = "Подключено к" + Properties.Settings.Default.ComName;
                    this.pictureBox1.Image = Properties.Resources.connected;
                }
                else
                {
                    this.Text = "Ошибка подключения к " + Properties.Settings.Default.ComName;
                    this.pictureBox1.Image = Properties.Resources.dscn;
                }
            }
            else
            {
                bool res = c.closeCOM();
                if(res)
                {
                    this.Text = "Отключено";
                    this.pictureBox1.Image = Properties.Resources.dscn;
                }
                else
                {
                    this.Text = "Ошибка подключения к " + Properties.Settings.Default.ComName;
                    this.pictureBox1.Image = Properties.Resources.dscn;
                }
                
            }
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }
        private int CurrentValueCell = -1;
        /// <summary>
        /// Обработчик кнопки "Включить измерение"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
                if (!isMesStarted && !IsCurrentCalOccur)
                {
                    byte[] b = { 0x11, 0x05, 0, 0, 0xFF, 0, 0, 0 };
                    int crc = c.CRC16(b, 6);
                    b[6] = (byte)(crc >> 8);
                    b[7] = (byte)(crc);
                    bool res = c.sendData(b, 8);
                    if (!res)
                    {
                        Button but = (Button)sender;
                        but.Text = "Ошибка при отправке";
                    }
                    autosave_timer.Start();
                   // List<Background> Slot = new List<Background>();
                   // CurrentValueCell++;
                    //this.label9.Text = String.Format("Измерение №{0}", CurrentValueCell+1);
                    //this.label3.Text = String.Format("Количество точек:{0}", 0);
                    //this.dataGridView1.Rows.Add(String.Format("Измерение №{0}", CurrentValueCell + 1), String.Format("{0}", CurrentValueCell + 1), true);
                    //output_buffer.Add(Slot);
                }
                else if (isMesStarted && !IsCurrentCalOccur)
                {
                    byte[] b = { 0x11, 0x05, 0, 0, 0, 0, 0, 0 };
                    int crc = c.CRC16(b, 6);
                    b[6] = (byte)(crc >> 8);
                    b[7] = (byte)(crc);
                    bool res = c.sendData(b, 8);
                    if (!res)
                    {
                        Button but = (Button)sender;
                        but.Text = "Ошибка при отправке";
                    }
                    autosave_timer.Stop();
                }
            
        }
        private List<double> Current = new List<double>();
        private double dA = 0; //Изменение тока на 1 единицу кода
        private void calculateCurrent()
        {
            dA = ((WindowsFormsApp1.Properties.Settings.Default.VREF4 / 256) * 1000000) / WindowsFormsApp1.Properties.Settings.Default.RShunt;
            for (int i = 0; i < 256; i++)
            {
                int rwb = (i * WindowsFormsApp1.Properties.Settings.Default.R_ab) / 256  + 2 * WindowsFormsApp1.Properties.Settings.Default.R_wiper;
                double vshunt = (rwb * WindowsFormsApp1.Properties.Settings.Default.VREF4 )/ WindowsFormsApp1.Properties.Settings.Default.R_ab;
                double current = (vshunt / WindowsFormsApp1.Properties.Settings.Default.RShunt) * 1000000;
                Current.Add(current);
            }
        }
        private bool BackgroundCal = false;
        /// <summary>
        /// Устанавливает/Обнуляет фон
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            /**Button b = (Button)sender;
            if (!bc.isSet)
            {
                bc.conductivity = LastSetVal.conductivity;
                bc.dvoltage = LastSetVal.dvoltage;
                bc.impedance = LastSetVal.impedance;
                bc.voltage = LastSetVal.voltage;
                this.button4.Text = "Обнулить Фон";
                bc.isSet = true;
            }else if(bc.isSet)
            {
                bc.voltage = 0;
                bc.dvoltage = 0;
                bc.impedance = 0;
                bc.conductivity = 0;
                bc.isSet = false;
                this.button4.Text = "Установить фон";
            }*/
            if (isMesStarted)
            {
                if (poll_timer.Enabled)
                {
                    poll_timer.Stop();
                    isTimerWasDisabled = true;
                }
                previous_code = sent_current;
                BackgroundCal = true;
                sendCurrent(0);
            }
            else
            {
                MessageBox.Show("Необходимо начать измерение!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Выводит csv файл с измерениями
        /// Не удаляет существующие измерения
        /// </summary>
        /// <param name="changeSeparator">Выбор разделителя, true - origin style</param>
        /// <param name="path">Путь</param>
        /// <param name="showResult">Выводить сообщение о сохранении или нет</param>
        /// <param name="MeasuresToImport">Индексы измерений, которые нужно сохранить</param>
        private void ExportMeasurment(bool changeSeparator, string path, bool showResult, List<int> MeasuresToImport)
        {
            try
            {
                for (int i = 0; i < output_buffer.Count; i++)
                {
                    if (MeasuresToImport.Contains(i))
                    {
                        List<string> outss = new List<string>();
                        string s = "Z(impedance om);ae(conductivity S/m);T(°С)";
                        if (changeSeparator)
                        {
                            s = s.Replace(',', '.');
                            s = s.Replace(';', ',');
                        }
                        outss.Add(s);
                        string filename = this.dataGridView1.Rows[i].Cells[1].Value.ToString();
                        foreach (Background b in output_buffer[i])
                        {
                            string line = String.Format("{0};{1};{2:F1}", b.impedance, b.conductivity, b.Temperater);
                            if (changeSeparator)
                            {
                                line = line.Replace(',', '.');
                                line = line.Replace(';', ',');
                            }
                            outss.Add(line);
                        }
                        String path_ch = path.Replace(".csv", String.Format("_{0}.csv", filename));
                        File.WriteAllLines(path_ch, outss); 
                    }
                }
                if(showResult)
                    MessageBox.Show("Файлы успешно сохранены", "Успешно", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception eee)
            {
                if (showResult)
                    MessageBox.Show("Невозможно сохранить в файлы. Ошибка:" + eee.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Вызывает форму сохранения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            if (!isMesStarted)
            {
                List<int> measurment_to_export = new List<int>();
                int itter = 0;
                if (this.dataGridView1.Rows.Count > 0)
                {
                    foreach (DataGridViewRow data in this.dataGridView1.Rows)
                    {
                        if (data.Cells[2].Value.Equals(true))
                        {
                            measurment_to_export.Add(itter);
                        }
                        itter++;
                    } 
                }
                if (measurment_to_export.Count > 0)
                {
                    string path = "";
                    SaveFileDialog f = new SaveFileDialog();
                    f.Filter = "csv files (*.csv)|*.csv";
                    f.FilterIndex = 1;
                    f.RestoreDirectory = true;
                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        path = f.FileName;
                    }
                    bool changeSeparator = false;
                    if (MessageBox.Show("Сменить стиль csv на стиль для OriginLab? (По умолчанию вывод в стиль Excel) ", "Успешно", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        changeSeparator = true;
                    }
                    ExportMeasurment(changeSeparator, path, true, measurment_to_export);
                }
                else
                {
                    MessageBox.Show("Необходимо выбрать хотя бы одно измерение!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Невозможно сохранить результат во время измерения!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
       /// <summary>
       /// Обрабатывает двойной клик по надписи "Режим"
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void label6_Click(object sender, EventArgs e)
        {
            /*if (!Properties.Settings.Default.isModeAuto)
            {
                Properties.Settings.Default.isModeAuto = true;
                this.label6.Text = "Режим: Авто";
            }
            else
            {
                Properties.Settings.Default.isModeAuto = false;
                this.label6.Text = "Режим: Ручной";
            }*/
        }

        private void label4_Click(object sender, EventArgs e)
        {
            sendCurrent(127);
        }
        /// <summary>
        /// Возвращает либо калиброванный ток, либо расчетный  
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private double getCurrent(int code)
        {
            if(Properties.Settings.Default.Cal_Intercept > 0 && Properties.Settings.Default.Cal_slope > 0 && code > 0) //проверяем параметры
            {
                if (256 > code + 1)
                {
                    double current = Properties.Settings.Default.Cal_Intercept + Properties.Settings.Default.Cal_slope * code;
                    return current * 1000000; //возвращает ток в мкА
                }
                else
                {
                    return Current[255];
                }
            }
            else
            {
                if (code > -1 && code < 256)
                {
                    return Current[code];
                }
                else
                {
                    return Current[255];
                }
            }
        }
        /// <summary>
        /// Читает сохраненную калибровку
        /// </summary>
       /* private void ReadCalCurrent()
        {
            string json = Properties.Settings.Default.CurrentCal;
            if (!json.Equals(""))
            {
                RealVoltage = JsonConvert.DeserializeObject<List<double>>(json);
            }
        }*/
        /// <summary>
        /// Рассчитывает коэфичиент граббса
        /// </summary>
        /// <param name="n">размер выборки</param>
        /// <returns>Коэфициент</returns>
        private double getGrubbsCoef(int n)
        {
           double coef = 1.68122 + 0.04616*n - 4.79085E-4*Math.Pow(n,2) + 1.67503E-6 * Math.Pow(n, 3);
            return coef;
        }
        /// <summary>
        /// Обрабатывает двойное нажатие на ток
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label4_DoubleClick(object sender, EventArgs e)
        {
            Form5 form = new Form5(RealVoltage);
            form.crch += Form_crch;
            Properties.Settings.Default.isModeAuto = false;
            Action v = () => this.label6.Text = "Режим: Ручной";
            Invoke(v);
            form.Show();
        }
        /// <summary>
        /// Обрабатывает отправку введеного тока
        /// </summary>
        /// <param name="e"></param>
        private void Form_crch(currentChangeArgs e)
        {
            int code = (int)Math.Floor(e.current/dA);
            if(code != 216 && code < 256 && code > 0)
            {
                sendCurrent((byte)code);
            }else if(code == 216)
            {
                sendCurrent(217);
            }
        }
        bool isPolarMes = false;
        private int Pol_itter = 0;
        private List<double> PolPlot = new List<double>();
        public event PolDataProfit PlDP;
        public event PolDataProfit PolAddToGraph;
        public delegate void PolDataProfit(PolDataProfite e);
        /// <summary>
        /// Обрабатывает нажатие кнопки "Поляризационные кривые"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            if (!IsCurrentCalOccur && !BackgroundCal)
            {
                Form6 form = new Form6();
                form.Strt += Form_Strt;
                form.Stop += Form_Stop;
                PlDP += form.Form1_PlDP;
                Graph graph = new Graph();
                form.ChangeGraphState += graph.State_change;
                form.ChartBeClosed += graph.Disable;
                form.ClearSeries += graph.ClearSeries;
                PolAddToGraph += graph.Form1_PlDP;
                form.Show();
                graph.Show();
            }
                      
        }

       /// <summary>
       /// Обрабатывает отмену поляризационной кривой
       /// </summary>
       /// <param name="e"></param>
        private void Form_Stop(StartArgs e)
        {
            //CalibrationTimer.Stop();
            Thread.Sleep(500);
            isPolarMes = false;
            Pol_itter = 0;
            sendCurrent(previous_code);
            if (isTimerWasDisabled)
            {
                poll_timer.Start();
            }
            if (MessageBox.Show("Сохранить кривую?", "Успешно", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                ExportPolCsv();
            }
        }
        private double CutOffVoltage = 2;
        private double CurrentInc = 1;
        private string pol_filename = "pol.csv";
        /// <summary>
        /// Обрабатывает начало пол кривой
        /// </summary>
        /// <param name="e"></param>
        private void Form_Strt(StartArgs e)
        {
            PolPlot.Clear();
            previous_code = sent_current;
            if (isMesStarted)
            {
                int interval = 1000;
                CutOffVoltage = e.CutoffVoltage;
                pol_filename = e.FileName + ".csv";
                if (e.Speed < 1)
                {
                    CurrentInc = 1;
                    interval *= (int)Math.Round(1 / e.Speed);
                }
                else
                {
                    CurrentInc = e.Speed * 1 / dA;
                }
                
                if (poll_timer.Enabled)
                {
                    poll_timer.Stop();
                    isTimerWasDisabled = true;
                }
                Thread.Sleep(500);//Пропустим все новые данные
                isPolarMes = true; //Включаем режим постройки кривой
                //CalibrationTimer = new System.Timers.Timer(200);
                //CalibrationTimer.Interval = interval/5;
                //CalibrationTimer.AutoReset = true;
                //CalibrationTimer.Elapsed += CalibrationTimer_Elapsed;
                sendCurrent(0); //Отправляем нулевой ток 
            }
            else
            {
                MessageBox.Show("Необходимо начать измерение!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Экспортирует поляризационную кривую в файл
        /// </summary>
        private void ExportPolCsv()
        {
            List<string> text = new List<string>();
            text.Add("V;A;j");
            int i = 0;
            foreach (double b in PolPlot)
            {
                double cur = getCurrent(i);
                string line = String.Format("{0:F5};{1:F5};{2:F5}", b, cur, cur*Math.Pow(10, -6) / (Properties.Settings.Default.radius_el * Properties.Settings.Default.lenght_el * 100));
                text.Add(line);
                i += (int)Math.Round(CurrentInc);
            }
            try
            {
                if (File.Exists(pol_filename))
                {

                    if (MessageBox.Show("Файл уже существует! Заменить?", "Ошибка", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                    {
                        File.WriteAllLines(pol_filename, text);
                    }
                    else
                    {
                        bool switcher = true;
                        for (int itt = 1; itt < 256 && switcher; itt++ )
                        {
                            string path = pol_filename.Replace(".csv", itt + ".csv");
                            if (!File.Exists(path))
                            {
                                File.WriteAllLines(path, text);
                                switcher = false;
                            }
                        }
                    }
                }
                else
                {
                    File.WriteAllLines(pol_filename, text);
                }
            }catch(Exception FileWrite)
            {
                if(MessageBox.Show("Невозможно сохранить файл!"+ FileWrite.Message, "Ошибка", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
                {
                    File.WriteAllLines(pol_filename, text);
                }
            }
        }

        private int CTIRL_itterator = -1;
        private int last_code = 127;
        /// <summary>
        /// Возращает константу сосуда
        /// </summary>
        /// <returns>константа сосуда</returns>
        private double getConstant()
        {
            double constant = Properties.Settings.Default.lenght_btw_el / (Properties.Settings.Default.lenght_el * Properties.Settings.Default.radius_el);
            return constant;
        }
        public static bool IsVDDACalibrated = false;
        private List<double> VoltageValue = new List<double>();
        private List<double> CurrentValue = new List<double>();
        /// <summary>
        /// Управляет измерением проводимости
        /// </summary>
        /// <param name="result">Усредненные значения с МК</param>
        /// <param name="src">Источник, откуда вызывается функция, 0 - из обычного источника, 1 - из ивента</param>
        private void CalculateTanOfIRLine(double result, int src)
        {
            if(result == -1 && src == 0) //начало измеренния
            {
                last_code = 127;
                CTIRL_itterator = 0;
                if(sent_current < 39)
                {
                    last_code = 40;
                }
                if (last_code > 215)
                {
                    last_code = 215;
                }
                VoltageValue.Clear();
                CurrentValue.Clear();
                sendCurrent((byte)(last_code - 39));
                poll_timer.Start();
            }

            if (CTIRL_itterator == 6 && src == 1) //конец итерации
            {
                Tuple<double, double> res = new Tuple<double, double>(result, getCurrent(sent_current - 3 + CTIRL_itterator));
                VoltageValue.Add(Properties.Settings.Default.VREF3 - result * (Communication.CurrentVDDA / 4095));
                CurrentValue.Add(getCurrent(last_code - 39 + CTIRL_itterator * 13) / 1000000);
                poll_timer.Stop();
                //Тут надо считать тангенс и вызывать update 
                Tuple<double, double> intercept_slope = Fit.Line(CurrentValue.ToArray<double>(), VoltageValue.ToArray<double>());
                Background b = new Background();
                b.impedance = intercept_slope.Item2;
                b.voltage = Properties.Settings.Default.VREF3 - VoltageValue.Average();
                b.dvoltage = VoltageValue.Average();
                b.Current = getCurrent((byte)last_code);
                b.conductivity = getConstant()/intercept_slope.Item2;
                Update_timer_Elapsed(b);
                CTIRL_itterator = 0;
                src = 0;
                VoltageValue.Clear();
                CurrentValue.Clear();
                sendCurrent((byte)(last_code - 39));
                poll_timer.Start();
            }
            if (CTIRL_itterator > -1 && CTIRL_itterator < 6 && src == 1) //итерация
            {
               // if(Math.Abs(sent_current - last_code) > 39 )
               // {
                   // last_code = sent_current;
                //}
                //Tuple<double, double> res = new Tuple<double, double>(result, getCurrent(sent_current - 3 + CTIRL_itterator));
                VoltageValue.Add(Properties.Settings.Default.VREF3 - result * (Communication.CurrentVDDA / 4095));
                CurrentValue.Add(getCurrent(last_code - 39 + CTIRL_itterator * 13) / 1000000);
                CTIRL_itterator++;
                sendCurrent((byte)(last_code - 39 + CTIRL_itterator*13));
            }
        }
        /// <summary>
        /// Выключает измерение, если пользователь закрывает программу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Закрыть программу? Все несохраненные данные будут потеряны.", "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                byte[] b = { 0x11, 0x05, 0, 0, 0, 0, 0, 0 };
                int crc = c.CRC16(b, 6);
                b[6] = (byte)(crc >> 8);
                b[7] = (byte)(crc);
                c.sendData(b, 8);
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            if (!c.isCOMopen())
            {
                bool comres = c.setupCOM();
                if (comres)
                {
                    this.Text = "Подключено к " + Properties.Settings.Default.ComName;
                    this.pictureBox1.Image = Properties.Resources.connected;
                }
                else
                {
                    this.Text = "Ошибка подключения к " + Properties.Settings.Default.ComName;
                    this.pictureBox1.Image = Properties.Resources.dscn;
                }
            }
            else
            {
                bool res = c.closeCOM();
                if (res)
                {
                    this.Text = "Отключено";
                    this.pictureBox1.Image = Properties.Resources.dscn;
                }
                else
                {
                    this.Text = "Ошибка подключения к " + Properties.Settings.Default.ComName;
                    this.pictureBox1.Image = Properties.Resources.dscn;
                }

            }
        }
    }
    public class PolDataProfite : EventArgs
    {
        public int itteration { get; set; }
        public int PointAmmount { get; set; }
        public double current { get; set; }
        public double v { get; set; }
    }
        public class PresetObj
    {
        public string Name { get; set; } //Имя пресета
        public int Lowest_Range { get; set; } //Нижний допустимый код, связаный с током в массиве Current
        public int Higest_Range { get; set; } //Самый высокий допустимый код, связаный с током массиве Current
        public int Recomended_Range { get; set; }
        public int IndexInComBox { get; set; } //Индекс в combobox1
    }
    public class Background
    {
        public double voltage { get; set; }
        public double dvoltage { get; set; }
        public double impedance { get; set; }
        public double conductivity { get; set; }
        public bool isSet { get; set; }
        public double Current { get; set; }
        public double Temperater { get; set; }
    }
}
