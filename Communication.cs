﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Communication
    {
        private SerialPort serialPort = new SerialPort();
        public List<byte> SP_rx_buffer = new List<byte>();
        public event MesureStatusChange msch;
        public event ProfitData profit_data;
        public event CurrentDataRecived cd_resived;
        public static double CurrentVDDA = 3.3;
        public delegate void MesureStatusChange(MesureStatusChangeArgs e);
        public delegate void ProfitData(ProfitDataArgs e);
        public delegate void CurrentDataRecived(ProfitDataArgs e);
        // Расчет CRC16 для проверки пакетов ModBusRTU
        // для добавления в пакет старший и младший байты должны быть поменяны местами
        private readonly ushort[] crc16Table = new ushort[]
        {
        0x0000, 0xC1C0, 0x81C1, 0x4001, 0x01C3, 0xC003, 0x8002, 0x41C2,
        0x01C6, 0xC006, 0x8007, 0x41C7, 0x0005, 0xC1C5, 0x81C4, 0x4004,
        0x01CC, 0xC00C, 0x800D, 0x41CD, 0x000F, 0xC1CF, 0x81CE, 0x400E,
        0x000A, 0xC1CA, 0x81CB, 0x400B, 0x01C9, 0xC009, 0x8008, 0x41C8,
        0x01D8, 0xC018, 0x8019, 0x41D9, 0x001B, 0xC1DB, 0x81DA, 0x401A,
        0x001E, 0xC1DE, 0x81DF, 0x401F, 0x01DD, 0xC01D, 0x801C, 0x41DC,
        0x0014, 0xC1D4, 0x81D5, 0x4015, 0x01D7, 0xC017, 0x8016, 0x41D6,
        0x01D2, 0xC012, 0x8013, 0x41D3, 0x0011, 0xC1D1, 0x81D0, 0x4010,
        0x01F0, 0xC030, 0x8031, 0x41F1, 0x0033, 0xC1F3, 0x81F2, 0x4032,
        0x0036, 0xC1F6, 0x81F7, 0x4037, 0x01F5, 0xC035, 0x8034, 0x41F4,
        0x003C, 0xC1FC, 0x81FD, 0x403D, 0x01FF, 0xC03F, 0x803E, 0x41FE,
        0x01FA, 0xC03A, 0x803B, 0x41FB, 0x0039, 0xC1F9, 0x81F8, 0x4038,
        0x0028, 0xC1E8, 0x81E9, 0x4029, 0x01EB, 0xC02B, 0x802A, 0x41EA,
        0x01EE, 0xC02E, 0x802F, 0x41EF, 0x002D, 0xC1ED, 0x81EC, 0x402C,
        0x01E4, 0xC024, 0x8025, 0x41E5, 0x0027, 0xC1E7, 0x81E6, 0x4026,
        0x0022, 0xC1E2, 0x81E3, 0x4023, 0x01E1, 0xC021, 0x8020, 0x41E0,
        0x01A0, 0xC060, 0x8061, 0x41A1, 0x0063, 0xC1A3, 0x81A2, 0x4062,
        0x0066, 0xC1A6, 0x81A7, 0x4067, 0x01A5, 0xC065, 0x8064, 0x41A4,
        0x006C, 0xC1AC, 0x81AD, 0x406D, 0x01AF, 0xC06F, 0x806E, 0x41AE,
        0x01AA, 0xC06A, 0x806B, 0x41AB, 0x0069, 0xC1A9, 0x81A8, 0x4068,
        0x0078, 0xC1B8, 0x81B9, 0x4079, 0x01BB, 0xC07B, 0x807A, 0x41BA,
        0x01BE, 0xC07E, 0x807F, 0x41BF, 0x007D, 0xC1BD, 0x81BC, 0x407C,
        0x01B4, 0xC074, 0x8075, 0x41B5, 0x0077, 0xC1B7, 0x81B6, 0x4076,
        0x0072, 0xC1B2, 0x81B3, 0x4073, 0x01B1, 0xC071, 0x8070, 0x41B0,
        0x0050, 0xC190, 0x8191, 0x4051, 0x0193, 0xC053, 0x8052, 0x4192,
        0x0196, 0xC056, 0x8057, 0x4197, 0x0055, 0xC195, 0x8194, 0x4054,
        0x019C, 0xC05C, 0x805D, 0x419D, 0x005F, 0xC19F, 0x819E, 0x405E,
        0x005A, 0xC19A, 0x819B, 0x405B, 0x0199, 0xC059, 0x8058, 0x4198,
        0x0188, 0xC048, 0x8049, 0x4189, 0x004B, 0xC18B, 0x818A, 0x404A,
        0x004E, 0xC18E, 0x818F, 0x404F, 0x018D, 0xC04D, 0x804C, 0x418C,
        0x0044, 0xC184, 0x8185, 0x4045, 0x0187, 0xC047, 0x8046, 0x4186,
        0x0182, 0xC042, 0x8043, 0x4183, 0x0041, 0xC181, 0x8180, 0x4040
        };
        /// <summary>
        /// Открывает поток COM порта, устанавливает обработчики событий
        /// </summary>
        /// <returns> true в случае успеха</returns>
        public bool setupCOM()
        {
            serialPort.BaudRate = WindowsFormsApp1.Properties.Settings.Default.sp_baudrate;
            serialPort.DataBits = WindowsFormsApp1.Properties.Settings.Default.sp_datasize;
            serialPort.DiscardNull = false;
            serialPort.Handshake = Handshake.None;
            serialPort.StopBits = StopBits.One;
            serialPort.PortName = WindowsFormsApp1.Properties.Settings.Default.ComName;

            serialPort.DataReceived += SerialPort_DataReceived;
            serialPort.ErrorReceived += SerialPort_ErrorReceived;

            serialPort.ReadTimeout = 1000;
            serialPort.WriteTimeout = 1000;

            try
            {
                serialPort.Open();
                return true;
            }catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                return false;
            }
        }
       /// <summary>
       /// Расчет CRC16 modbus с полиномом 0xFFFF
       /// </summary>
       /// <param name="bytes">массив данных</param>
       /// <param name="len">Размер массива</param>
       /// <returns></returns>
        public ushort CRC16(byte[] bytes, int len)
        {
            ushort crc = 0xFFFF;
            for (var i = 0; i < len; i++)
                crc = (ushort)((crc << 8) ^ crc16Table[(crc >> 8) ^ bytes[i]]);
            return crc;
        }

        private void SerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            
        }

        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {   
            SerialPort sp = (SerialPort)sender;
            int readsize = sp.BytesToRead;
            byte[] rx =  new byte[readsize];
            sp.Read(rx, 0 , readsize);
            SP_rx_buffer.AddRange(rx);
            HandleModbus();
        }
        /// <summary>
        /// Возращает состояние порта
        /// </summary>
        /// <returns></returns>
        public bool isCOMopen()
        {
            return serialPort.IsOpen;
        }
        /// <summary>
        /// Закрывает открытый порт
        /// </summary>
        /// <returns> true в случае успеха, false если порт был закрыл или произошла ошибка</returns>
        public bool closeCOM()
        {
            try
            {
                serialPort.Close();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }
        /// <summary>
        /// Обработчик, который сортирует modbus фрэймы в буфере
        /// </summary>
        public void HandleModbus()
        {
            List<int> SlaveId_entry = new List<int>();
            for (int i = 0; i < SP_rx_buffer.Count; i++)
            {
                if (SP_rx_buffer[i].Equals(WindowsFormsApp1.Properties.Settings.Default.SlaveID))
                {
                    SlaveId_entry.Add(i);
                }
            }
            if (SlaveId_entry.Count > 0)
            {
                List<List<byte>> crc = new List<List<byte>>();
                for (int i = 0; i < SlaveId_entry.Count; i++)
                {
                    if (i != SlaveId_entry.Count - 1)
                    {
                        int size = SlaveId_entry[i + 1] - SlaveId_entry[i];
                        byte[] temp = new byte[size];
                        SP_rx_buffer.CopyTo(SlaveId_entry[i], temp, 0, size);
                        crc.Add(temp.ToList<byte>());
                    }
                    else if (i == SlaveId_entry.Count - 1)
                    {
                        int size = SP_rx_buffer.Count - SlaveId_entry[i];
                        byte[] temp = new byte[size];
                        SP_rx_buffer.CopyTo(SlaveId_entry[i], temp, 0, size);
                        crc.Add(temp.ToList<byte>());
                    }
                }
                int before_size = SP_rx_buffer.Count;
                for (int ind = 0; ind < crc.Count; ind++)
                {
                    if (crc[ind].Count > 4) //В принципе, нет ни одного ответа на команду модбаса меньше 4 байт, поэтому введем такое ограничение
                    {
                        int crc16 = CRC16(crc[ind].ToArray<byte>(), crc[ind].Count - 2);
                        if (crc16 == (crc[ind][crc[ind].Count - 2] << 8 | crc[ind][crc[ind].Count - 1]))
                        {
                            int rem_size = 0;
                            if (ind != crc.Count - 1)
                            {
                                rem_size = SlaveId_entry[ind + 1] - SlaveId_entry[ind];
                            }
                            if (ind == crc.Count - 1)
                            {
                                rem_size = before_size - SlaveId_entry[ind];
                            }
                            SP_rx_buffer.RemoveRange(SlaveId_entry[0], rem_size);
                            HandleModbusCommands(crc[ind]);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Обрабатывает команды mobdus
        /// </summary>
        /// <param name="rx">массив с полным фрэймом</param>
        void HandleModbusCommands(List<byte> rx)
        {
            switch(rx[1])
            {
                case 0x04:
                    int size = rx[2];
                    if((rx.Count - 3 - 2) == size)
                    {
                        int[] data = new int[size / 2];
                        for(int i = 0; i < size; i+=2)
                        {
                            data[i / 2] = rx[3 + i] << 8 | rx[3 + i + 1];
                        }
                       ProfitDataArgs e = new ProfitDataArgs();
                       e.data = data;
                       profit_data(e);
                        
                    }
                    break; 
                case 0x05: //вкл-выкл измерения
                    List<byte> enable_mes = new List<byte>() { 0x11, 0x05, 0, 0, 0xFF, 0, 0x8e, 0xaa};
                    List<byte> disable_mes = new List<byte>() { 0x11, 0x05, 0, 0, 0, 0, 0xcf, 0x5a };
                    if(rx.SequenceEqual(enable_mes))
                    {
                        MesureStatusChangeArgs eventArgs = new MesureStatusChangeArgs();
                        eventArgs.isMesEnable = true;
                        eventArgs.isMesDisable = false;
                        msch(eventArgs);
                    }
                    if(rx.SequenceEqual(disable_mes))
                    {
                        MesureStatusChangeArgs eventArgs = new MesureStatusChangeArgs();
                        eventArgs.isMesEnable = false;
                        eventArgs.isMesDisable = true;
                        msch(eventArgs);
                    }
                    break;
                case 0x06: //результат установки тока
                    if (rx.Count == 8)
                    {
                        byte recived_code = rx[5];
                        ProfitDataArgs p = new ProfitDataArgs();
                        int[] i = new int[2];
                        i[0] = (int)recived_code;
                        p.data = i;
                        cd_resived(p);
                    }
                    break;
                case 0x01: //проверяем ошибки
                    if (rx.Count == 7)
                    {
                        int register = rx[3] | rx[4] << 8;
                        if((register >> 3 & 1) == 1) //Электрод отключен или измеряемое значение говно
                        {
                            MesureStatusChangeArgs eventArgs = new MesureStatusChangeArgs();
                            eventArgs.isMesEnable = false;
                            eventArgs.isMesDisable = (register & 1) != 1;
                            eventArgs.error_code = 3;
                            msch(eventArgs);
                        }
                        if ((register >> 2 & 1) == 1) //проблемы с таск менеджером
                        {
                            MesureStatusChangeArgs eventArgs = new MesureStatusChangeArgs();
                            eventArgs.isMesEnable = false;
                            eventArgs.isMesDisable = false;
                            eventArgs.error_code = 2;
                            msch(eventArgs);
                        }
                        if ((register >> 1 & 1) == 1) //проблемы с И2С
                        {
                            MesureStatusChangeArgs eventArgs = new MesureStatusChangeArgs();
                            eventArgs.isMesEnable = false;
                            eventArgs.isMesDisable = false;
                            eventArgs.error_code = 1;
                            msch(eventArgs);
                        }
                    }
                        break;
            }
        }
        /// <summary>
        /// Отправляет байтовый массив в COM
        /// </summary>
        /// <param name="rx">Байтовый массив</param>
        /// <returns>true в случае успеха</returns>
        public bool sendData(byte[] rx, int size)
        {
            try
            {
                serialPort.Write(rx, 0, size);
                return true;
            }catch(Exception e)
            {
                Console.Write(e.Message);
                return false;
            }
        }
        
    }
    public class MesureStatusChangeArgs : EventArgs
    {
        public bool isMesEnable;
        public bool isMesDisable;
        public int error_code;
    }
    public class ProfitDataArgs : EventArgs
    {
        public int[] data;
    }
}
