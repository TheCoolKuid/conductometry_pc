﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace WindowsFormsApp1
{
    public partial class Graph : Form
    {
        Series series;
        public Graph()
        {
            InitializeComponent();
            this.chart1.Titles.Add("j/V");
            this.chart1.Series.Clear();
            series = this.chart1.Series.Add("j/V");
            series.ChartType = SeriesChartType.FastLine;
            series.Palette = ChartColorPalette.Excel;
            chart1.ChartAreas[0].AxisY.Title = "j A/дм2";
            chart1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
            chart1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
            chart1.Legends.Clear();
            chart1.ChartAreas[0].AxisX.Title = "V В";
            series.Enabled = true;
            series.BorderWidth = 7;
        }
        public void Form1_PlDP(PolDataProfite e)
        {
            Action v = () => series.Points.AddXY(e.v, (e.current * Math.Pow(10, -6)) / (Properties.Settings.Default.radius_el * Properties.Settings.Default.lenght_el * 100));
            try
            {
                Invoke(v);
            }
            catch (Exception eeee)
            {
                Console.Write(eeee.Message);
            }
        }
        public void State_change(StartArgs e)
        {
            if (e.State)
            {
                this.Show();
            }
            else
            {
                this.Hide();
            }
        }
        private bool isclosing = false;
        public void Disable(StartArgs e)
        {
            isclosing = true;
            this.Close();
        }

        public void ClearSeries(StartArgs e)
        {
            series.Points.Clear();
        }

        private void Graph_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isclosing)
            {
                e.Cancel = true;
            }
        }
    }
    }
