﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form5 : Form
    {
        public Form5(List<double> current)
        {
            InitializeComponent();
            vs = current;
        }
        public event currentChange crch;
        public delegate void currentChange(currentChangeArgs e);
        private List<double> vs = new List<double>();
        private void Form5_Load(object sender, EventArgs e)
        {
            WatchDog = new System.Timers.Timer(500);//В мс//Если пользователь в течении 500мс не меняет значение тока, то ток будет отправляться
            WatchDog.AutoReset = false;
            WatchDog.Elapsed += WatchDog_Elapsed;
            textBox1.Text = "127";
            this.label3.Text = "Я помогу выбрать оптимальное значение тока. Пожалуйста, введи ток и мы его обсудим.";
        }
        /// <summary>
        /// Обрабатывает тик таймера 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WatchDog_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            currentChangeArgs bebee = new currentChangeArgs();
            bebee.current = value;
            crch(bebee);
        }

        System.Timers.Timer WatchDog;
        private double value = 0;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            bool res = Double.TryParse(textBox1.Text, out value);
            if (res)
            {
                WatchDog.Stop();
                WatchDog.Start();
                if (value >= 0 && value <= vs[255])
                {
                    label3.ForeColor = Color.Black;
                    label4.Text = String.Format("V шунта: {0:F2} V", Properties.Settings.Default.RShunt * value / 1000000);
                    if (value > 200)
                    {
                        label5.Text = "Средне";
                        label5.ForeColor = Color.DarkOrange;
                        //label6.Text = "Рекомендуем режим для растворителей: Дист. вода, Ацетон абс., Этанол абс. ";
                        label3.Text = "\nОпасная, получается, ситуация. Рекомендуемое падение напряжения на растворе 1В. Суммарное падение напряжение на Шунте и Растворе " +
                            "не должно превышать 4В, учитывая, что кроме падения напряжения по закону Ома, раствор" +
                          "имеет собственный равновесный ЭХ. потенциал,";
                        if (value > 264 && value < 300)
                        {
                            label6.Text = "Рекомендуем режим для растворителей: Ацетон, Этанол, Водопроводная вода(А почему нет?)";
                        }
                        if (value > 203 && value < 300)
                        {
                            label6.Text = "Рекомендуем режим для растворителей: Этанол, Водопроводная вода(А почему нет?)";
                        }
                    }
                    if (value < 50)
                    {
                        label5.Text = "Плохо";
                        label5.ForeColor = Color.Red;
                        label6.Text = "Рекомендуем режим для растворителей: Дихлорэтан абс., Анилин абс.";
                        label3.Text = "\nТакое маленькое значение падения напряжение на шунте означает сильную неустойчивость к помехам." +
                           "Ближайшая светодиодная лампочка или работающий электродвигатель может навести помеху в половину" +
                           "установленного значения тока! (Хотя если рядом с вами находится устройство, способное навести помеху" +
                           "в 250мВ, советуем срочно бежать в безопасное место, возможно, это устройство - ядерный взрыв)";
                    }
                    if (value >= 50 && value <= 200)
                    {
                        label5.Text = "Хорошо";
                        label5.ForeColor = Color.DarkGreen;
                        if (value > 65 && value < 78)
                        {
                            label6.Text = "Рекомендуем режим для растворителей: Дихлорэтан абс.";
                        }
                        if (value > 105 && value < 126)
                        {
                            label6.Text = "Рекомендуем режим для растворителей: Дист. вода";
                        }
                        if (value > 50 && value < 67)
                        {
                            label6.Text = "Рекомендуем режим для растворителей: Анилин";
                        }
                        Random random = new Random();
                        int randomNumber = random.Next(0, 10);
                        switch (randomNumber)
                        {
                            case 0:
                                label3.Text = "Идеальный выбор! Возможно, в ваших руках прибор перестанет показывать длину в попугаях";
                                break;
                            case 1:
                                label3.Text = "А чем, собственно, не устроил автоматический режим?";
                                break;
                            case 2:
                                label3.Text = "Да, текст повторяется";
                                break;
                            case 3:
                                label3.Text = "В яблочко! Но в следующий раз нужно быть еще точнее";
                                break;
                            case 4:
                                label3.Text = "Придумывать подбадривающие комментарии сложнее, чем все это разрабатывать";
                                break;
                            case 5:
                                label3.Text = "Вы невероятно точны! Вы, верно, были снайпером?";
                                break;
                            case 6:
                                label3.Text = "Добро пожаловать в мой мир!";
                                break;
                            case 7:
                                label3.Text = "Выбор тока похож на выборы, что не выберешь - программа все равно решит иначе";
                                break;
                            case 8:
                                label3.Text = "Не маринуйтесь скукой! Выберите автоматический режим!";
                                break;
                            case 9:
                                label3.Text = "Я ток никогда не выбирал ,но в этот раз точно выберу за какой ток голосовать. Прибор от народа!";
                                break;
                        }
                    }
                }
                else
                {
                    if(value > vs[255])
                    {
                        value = vs[254];
                        textBox1.Text = String.Format("{0:f0}", value);
                    }
                    if (value < 0)
                    {
                        value = 0;
                        textBox1.Text = "0";
                    }
                }


            }
            else
            {
                Random random = new Random();
                int randomNumber = random.Next(0, 10);
                label3.ForeColor = Color.DarkRed;
                switch (randomNumber)
                {
                    case 0:
                        label3.Text = "В чем подвож? Сука у нее не цифры!";
                        break;
                    case 1:
                        label3.Text = "Не не я на это не куплюсь, давайте цифры";
                        break;
                    case 2:
                        label3.Text = "Тук-тук. Кто там? Не цифры";
                        break;
                    case 3:
                        label3.Text = "Все што я умею это четать цырфы";
                        break;
                    case 4:
                        label3.Text = "*Когда ввел не цифры* Выйди отсюда, розбiйник";
                        break;
                    case 5:
                        label3.Text = "За маму! За папу! ЗА ТО ЧТО НЕ ВВЕЛ ЦИФРЫ!";
                        break;
                    case 6:
                        label3.Text = "Я пришел сюда, чтобы вы ввели цифры и я написал ошибку. Как видите, ошибку я уже написал";
                        break;
                    case 7:
                        label3.Text = "УДОЛИ все, что не цифры";
                        break;
                    case 8:
                        label3.Text = "Нео, проснись, ты ввел не цифры";
                        break;
                    case 9:
                        label3.Text = "Цыфры (не введены)";
                        break;

                }
            }
        }
    }
    public class currentChangeArgs : EventArgs
    {
        public double current { get; set; }
    }

}
